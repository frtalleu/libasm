section .text

global ft_strlen

ft_strlen :
			mov rax, -1
increment :
			inc rax
			cmp byte [rdi + rax], 0
			jne increment
			ret
