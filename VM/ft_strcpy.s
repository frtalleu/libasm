section .text

global ft_strcpy

ft_strcpy :
			mov r8, -1
increment :
			inc r8
			mov r9b, byte [rsi + r8]
			mov byte [rdi + r8], r9b
			cmp byte [rsi + r8], 0
			jne increment
			mov rax, rdi
			ret
