section .text

extern __errno_location

global ft_write

ft_write :
			mov rax, 1
			syscall
			jne end
			push rax
			call __errno_location
			pop rdi
			neg rdi
			mov [rax], rdi 
			mov rax, -1
end :
		ret
