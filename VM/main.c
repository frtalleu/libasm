/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/07 12:13:50 by frtalleu          #+#    #+#             */
/*   Updated: 2020/09/22 14:44:07 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include "libasm.h"

# define LONG "this a fucking long string, i need this string only to make test with a very very very long string. I don't really know if the this of this string is enough or not"
# define SHORT "a"
# define EMPTY ""
# define TEST "un test normal"

void check_strlen(void)
{
	char *str = "STRLEN";
	printf("|----------------|\n");
	printf("|-----%s-----|\n", str);
	printf("|----------------|\n");
	if (ft_strlen(LONG) == strlen(LONG) && ft_strlen(SHORT) == strlen(SHORT)
		&& ft_strlen(EMPTY) == strlen(EMPTY) && ft_strlen(TEST) == ft_strlen(TEST))
		printf("%s IS GOOD 👍\n", str);
	else
		printf("%s IS NOT GOOD 👎\n", str);
}

void check_strcmp(void)
{
	char *str = "STRCMP";
	printf("|----------------|\n");
	printf("|-----%s-----|\n", str);
	printf("|----------------|\n");
	if(ft_strcmp(LONG, SHORT) != 0 && ft_strcmp(LONG, LONG) == 0
		&& ft_strcmp(EMPTY, EMPTY) == 0 && ft_strcmp("a", "b") < 0 && ft_strcmp("b", "a") > 0)
		printf("%s IS GOOD 👍\n", str);
	else
		printf("%s IS NOT GOOD 👎\n", str);
}

void check_strdup(void)
{
	char *str = "STRDUP";
	char *s1 = ft_strdup(LONG);
	char *s2 = ft_strdup(SHORT);
	char *s3 = ft_strdup(EMPTY);
	char *s4 = ft_strdup(TEST);
	printf("|----------------|\n");
	printf("|-----%s-----|\n",str);
	printf("|----------------|\n");
	if(strcmp(s1, LONG) == 0 && strcmp(s2, SHORT) == 0 
	&& strcmp(s3, EMPTY) == 0 && strcmp(s4, TEST) == 0)
	{
		printf("%s IS GOOD 👍\n", str);
		printf("%s is good only if the free gonna work\n", str);
		free(s1);
		free(s2);
		free(s3);
		free(s4);
		printf("perfect, the %s work\n", str);
	}
	else
		printf("%s IS NOT GOOD 👎\n", str);
}

void check_strcpy(void)
{
	char *str = "STRCPY";
	char *st = malloc(sizeof(char) * 1000);
	char *s;
	s = st;
	int i = 0;

	printf("|----------------|\n");
	printf("|-----%s-----|\n", str);
	printf("|----------------|\n");
	ft_strcpy(st, LONG);
	if (st != s || strcmp(st, LONG) != 0)
		i++;
	ft_strcpy(st, SHORT);
	if (st != s || strcmp(st, SHORT) != 0)
		i++;
	ft_strcpy(st, EMPTY);
	if (st != s || strcmp(st, EMPTY) != 0)
		i++;
	ft_strcpy(st, TEST);
	if (st != s || strcmp(st, TEST) != 0)
		i++;
	if(i == 0)
		printf("%s IS GOOD 👍\n", str);
	else
		printf("%s IS NOT GOOD 👎\n", str);
}

void check_write_read(void)
{
	char *str = "WRITE & READ";
	char *st = "READ";
	char st1[501];
	char st2[501];
	int i;
	int j;
	int file;

	printf("|----------------|\n");
	printf("|--%s--|\n", str);
	printf("|----------------|\n");
	file = open("./main.c", O_RDONLY);
	j = ft_read(file, st1, 500);	
	close(file);
	file = open("./main.c", O_RDONLY);
	i = read(file, st2, 500);
	st1[500] = '\0';	
	st2[500] = '\0';
	if (i != j || strcmp(st1, st2) != 0)
		printf("%s IS NOT GOOD 👎\n", st);
	else
		printf("%s IS GOOD 👍\n", st);
	printf("some visual test with write\n");
	write(1, LONG, strlen(LONG));
	write(1, "\n", 1);
	ft_write(1, LONG, strlen(LONG));
	write(1, "\n", 1);
	write(1, SHORT, strlen(SHORT));
	write(1, "\n", 1);
	ft_write(1, SHORT, strlen(SHORT));
	write(1, "\n", 1);
	write(1, EMPTY, strlen(EMPTY));
	write(1, "\n", 1);
	ft_write(0, EMPTY, strlen(EMPTY));
}

int main(int ac, char **av)
{
	check_strlen();
	printf("\n\n");
	check_strcpy();
	printf("\n\n");
	check_strcmp();
	printf("\n\n");
	check_strdup();
	printf("\n\n");
	check_write_read();
}
