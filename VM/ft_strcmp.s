section .text

global ft_strcmp

ft_strcmp :
			mov r8, -1
			mov r9, 0
			mov rax, 0
increment :
			inc r8
			mov r9b, byte [rsi + r8]
			mov al, byte [rdi + r8]
			cmp byte r9b, byte al
			jne end
			cmp al, 0
			je end
			jmp increment
end :	
		sub rax, r9
		ret
