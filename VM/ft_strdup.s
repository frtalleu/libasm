section .text

extern ft_strlen
extern ft_strcpy
extern malloc

global ft_strdup

ft_strdup :
				call ft_strlen
				push rdi
				mov rdi, rax
				call malloc
				cmp rax, 0
				je err
				pop r8
				mov rsi, r8
				mov rdi, rax
				call ft_strcpy
				ret

err :
		mov rax, 0
		ret
