section .text

extern _ft_strlen
extern _ft_strcpy
extern _malloc

global _ft_strdup

_ft_strdup :
				call _ft_strlen
				push rdi
				mov rdi, rax
				call _malloc
				cmp rax, 0
				je err
				pop r8
				mov rsi, r8
				mov rdi, rax
				call _ft_strcpy
				ret

err :
		mov rax, 0
		ret
