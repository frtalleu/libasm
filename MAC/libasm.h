/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libasm.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <frtalleu@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/09 13:33:21 by frtalleu          #+#    #+#             */
/*   Updated: 2020/09/09 13:46:44 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef _LIBASM_H
# define _LIBASM_H
# include <stdlib.h>

size_t	ft_strlen(char *str);
char	ft_strcpy(char *s1, char *s2);
int		ft_strcmp(char *s1, char *s2);
int		ft_write(int fd, void *buf, size_t nb_bytes);
int		ft_read(int fd, void *buf, size_t nb_bytes);
char	*ft_strdup(char *str);

#endif
