section .text

extern ___error

global _ft_read

_ft_read :
			mov rax, 0x2000003
			syscall
			jne end
			push rax
			call ___error
			pop rdi
			neg rdi
			mov [rax], rdi 
			mov rax, -1
end :
		ret
