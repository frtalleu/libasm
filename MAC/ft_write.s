section .text

extern ___error

global _ft_write

_ft_write :
			mov rax, 0x2000004
			syscall
			js end
			push rax
			call ___error
			pop rdi
			neg rdi
			mov [rax], rdi 
			mov rax, -1
end :
		ret
